#!/bin/sh

# Build the RMI Client and Server

# 1: CLEAN
rm -Rf bin/*.class

# 2: JAVAC
javac -d bin src/*.java

# 3: RMIC
cd bin
rmic SimpleServer
