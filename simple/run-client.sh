#!/bin/sh

# Run SimpleClient (bin/SimpleClient.class) with the security
# policy wideopen.policy in this folder (allows sockets)
cd bin
java -Djava.security.policy=../wideopen.policy SimpleClient $1 $2