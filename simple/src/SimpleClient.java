import java.rmi.RMISecurityManager;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class SimpleClient {

    public static void main(String[] args) 
    {
        String message = "nothing-yet";
        
        System.setSecurityManager(new RMISecurityManager());
        
        try
        {
            String host = args.length > 0 ? args[0] : "127.0.0.1";
            String port = args.length > 1 ? args[1] : "";
            String name = "SimpleServer";
            String uri = "//" + host;
            if (!port.equalsIgnoreCase(""))
                uri+=":"+port; 
            uri += "/" + name;
            System.out.println("SimpleClient Connecting to: "+uri);
            HelloRemote obj = (HelloRemote) Naming.lookup(uri);
            message = obj.Hello();
            System.out.println("Message: "+message);
        }
        catch(Exception e)
        {
            System.err.println("SimpleClient exception:");
            e.printStackTrace();
        }
    }
    
}
