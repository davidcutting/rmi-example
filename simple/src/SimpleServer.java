import java.rmi.RemoteException;
import java.rmi.Naming;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class SimpleServer extends UnicastRemoteObject implements HelloRemote {

    public SimpleServer() throws RemoteException { }
    
    public String Hello() { return "Hello there!"; }
    
    public static void main(String args[])
    {
        String hostname = args.length > 0 ? args[0] : "127.0.0.1";
        String port = args.length > 1 ? args[1] : "";
        String uri = "";
        String name = "SimpleServer";

        System.out.println("SimpleServer Starting for hostname: "+hostname);

        // Doesn't seem needed now with explicit setting in URI
        //System.setProperty("java.rmi.server.hostname",hostname);

        if (!hostname.equalsIgnoreCase(""))
        {
            uri = "//" + hostname;
            if (!port.equalsIgnoreCase(""))
            {
                uri += ":" + port;
            }
            uri += "/";
        }
        uri += name;
        System.out.println("RMI Server URI: "+uri);

        try
        {
            SimpleServer obj = new SimpleServer();
            Naming.rebind(uri, obj);
            //Registry reg = LocateRegistry.getRegistry();
            //reg.rebind("SimpleServer",obj);
        }
        catch(Exception e)
        {
            System.err.println("SimpleServer exception:");
            e.printStackTrace();
        }
    }
    
}
