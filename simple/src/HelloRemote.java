import java.rmi.*;

public interface HelloRemote extends java.rmi.Remote {
    String Hello() throws RemoteException;
}
