#!/bin/sh

# Run the server and note the pid
cd bin
java SimpleServer $1 $2 & echo $! > ../run/SimpleServer.pid
