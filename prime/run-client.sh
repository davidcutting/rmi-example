#!/bin/bash

# Run Client (bin/Client.class) with the security
# policy wideopen.policy in this folder (allows sockets)
cd bin
java -Djava.security.policy=../wideopen.policy Client ${@:1}