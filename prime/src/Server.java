import java.rmi.RemoteException;
import java.rmi.Naming;
import java.rmi.server.UnicastRemoteObject;

public class Server extends UnicastRemoteObject implements PrimeRemote {

    public Server() throws RemoteException { }
    
    public boolean isPrime(int n) 
    {
        if (n<2) return true;
        if (n%2==0) return false;
        for (int i=3; i<(n-1); ++i)
            if (n%i == 0)
                return false;
        return true;

    }
    
    public static void main(String args[])
    {
        String hostname = args.length > 0 ? args[0] : "127.0.0.1";
        if (hostname.equalsIgnoreCase("--auto"))
            hostname = APIServices.RemoteIP();
        String port = args.length > 1 ? args[1] : "";
        String uri = "";
        String name = "PrimeServer";

        System.out.println("Server Starting for hostname: "+hostname+", name: "+name);

        // Doesn't seem needed now with explicit setting in URI
        //System.setProperty("java.rmi.server.hostname",hostname);

        if (!hostname.equalsIgnoreCase(""))
        {
            uri = "//" + hostname;
            if (!port.equalsIgnoreCase(""))
            {
                uri += ":" + port;
            }
            uri += "/";
        }
        uri += name;
        System.out.println("RMI Server URI: "+uri);

        try
        {
            Server obj = new Server();
            Naming.rebind(uri, obj);
        }
        catch(Exception e)
        {
            System.err.println("SimpleServer exception:");
            e.printStackTrace();
        }
    }
    
}
