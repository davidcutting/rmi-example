import java.rmi.RMISecurityManager;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.List;
import java.util.ArrayList;

public class Client {

    public int[] data;
    private int current = 0;
    private int finish = 0;

    public class DataType
    {
        public final static int FINISHED = -3;
        public final static int UNCOMPUTED = -2;
        public final static int PROGRESS = -1;
    }

    public class ClientWorker extends Thread
    {
        public String uri = "";
        public Client client = null;
        public String name = "PrimeServer";

        ClientWorker(String connect, Client parent)
        {
            uri = "//"+connect+"/"+name;
            client = parent;
        }

        public void run()
        {
            try
            {
                System.out.println("Client Connecting to: "+uri);
                PrimeRemote obj = (PrimeRemote) Naming.lookup(uri);

                int n = client.getNext();
                while (n != DataType.FINISHED)
                {
                    boolean r = obj.isPrime(n);
                    client.result(n, r);
                    n = client.getNext();
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            System.out.println("Worker Thread Finished: "+uri);
        }
    }

    public static void main(String [] args)
    {
        long sT = System.nanoTime();
        Client c = new Client(args);
        long eT = System.nanoTime();
        long ms = (eT - sT)/1000000;
        System.out.println("Time: "+ms+" ms");
    }

    synchronized public int getNext()
    {
        if (current >= finish)
            return DataType.FINISHED;
        data[current]=DataType.PROGRESS;
        return current++;
    }

    synchronized public void result(int i, boolean r)
    {
        if (r)
            data[i]=1;
        data[i]=0;
        System.out.println("Result for n="+i+",prime="+r);
    }

    public Client(String[] args) 
    {
        // Arguments:
        // Start-n
        // Finish-n
        // IP:Port ...
        

        System.setSecurityManager(new RMISecurityManager());
        
        if (args.length < 3)
        {
            System.out.println("Usage: java Client startN endN server1:port1 [server2:port2...]");
            System.exit(-1);
        }


        try
        {
            int startN = Integer.parseInt(args[0]);
            int finishN = Integer.parseInt(args[1]);
            finish = finishN;
            data = new int[finishN+1];
            current = startN;
            for (int i=startN; i<=finishN; ++i)
                data[i]=DataType.UNCOMPUTED;

            List<ClientWorker> clients = new ArrayList<ClientWorker>();
            for (int a=2; a<args.length; ++a)
            {
                ClientWorker cw = new ClientWorker(args[a],this);
                clients.add(cw);
                cw.start();
            }

            boolean quit = false;
            while(!quit)
            {
                if (current >= finish)
                    quit=true;
                else
                    Thread.sleep(100);
            }
        }
        catch(Exception e)
        {
            System.err.println("Client exception:");
            e.printStackTrace();
        }
    }
    
}
