import java.net.*;
import java.io.*;

class APIServices
{

	public static String RemoteIP()
	{
		String ip = "";
		try
		{
			String provider = "http://checkip.amazonaws.com";
			URL ipURL = new URL(provider);
			BufferedReader in = new BufferedReader(
				new InputStreamReader(
					ipURL.openStream()));

			ip = in.readLine();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return ip;
	}

}