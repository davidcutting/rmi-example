import java.rmi.*;

public interface PrimeRemote extends java.rmi.Remote {
    boolean isPrime(int n) throws RemoteException;
}
