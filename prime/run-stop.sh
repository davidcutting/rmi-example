#!/bin/sh

# End-to-end Demo
./run-registry.sh
sleep 1
./run-server.sh
sleep 1

./run-client.sh

./stop-server.sh
./stop-registry.sh
