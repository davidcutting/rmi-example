#!/bin/sh

# Run the RMI registry in the directory: run-registry.sh [port]
cd bin
rmiregistry $1 & echo $! > ../run/rmiregistry.pid
