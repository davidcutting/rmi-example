#!/bin/sh

# Run the server and note the pid
cd bin
java Server $1 $2 & echo $! > ../run/Server.pid
